#!/usr/bin/env bash

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 1; }
try() { "$@" || die "cannot $*"; }

#M_DEFAULT_TARGET_DIR="~/cloud/limos/vimazeno"
#M_DEFAULT_CERT_DIR="~/cloud/limos/cri/certs"
#M_DEFAULT_RECIPIENT="vincent.mazenod@isima.fr"

C="FR"
ST="Puy De Dome"
L="Aubiere"
O="Universite Clermont Auvergne"
OU="LIMOS"
VAULT_PATH="certificates"

SCRIPT_NAME="m"
CONFIG="`dirname $0`/config.json"

usage() {
  echo -e "\e[32mperform various admin tasks\e[0m"
  echo ""
  echo -e "\e[34mbackup securely (pgp)\e[0m"
  echo "  ${SCRIPT_NAME} backup ~/.ssh $M_DEFAULT_TARGET_DIR/ssh.tgz.gpg"
  echo "  ${SCRIPT_NAME} backup ~/.ssh $M_DEFAULT_TARGET_DIR/ssh.tgz.gpg --recipient $M_DEFAULT_RECIPIENT"
  echo -e "\e[35m or with config.json\e[0m"
  echo "  ${SCRIPT_NAME} backup ssh"
  echo "  ${SCRIPT_NAME} backupall"
  echo ""
  echo -e "\e[34mrestore securely (pgp)\e[0m"
  echo "  ${SCRIPT_NAME} restore $M_DEFAULT_TARGET_DIR/ssh.tgz.gpg $M_DEFAULT_SRC_DIR.ssh"
  echo -e "\e[35m or with config.json\e[0m"
  echo "  ${SCRIPT_NAME} restore ssh"
  echo "  ${SCRIPT_NAME} restoreall"
  echo ""
  echo -e "\e[34mmanage tls certs\e[0m"
  echo "  ${SCRIPT_NAME} cert create my.site.com --path $M_DEFAULT_CERT_DIR/"
  echo "  ${SCRIPT_NAME} cert vault my.site.com --path $M_DEFAULT_CERT_DIR/"
  echo "  ${SCRIPT_NAME} cert haproxy my.site.com --path $M_DEFAULT_CERT_DIR/ --rpath rproxy-isima:/etc/certs/"
  echo "  ${SCRIPT_NAME} cert nginx my.site.com --path $M_DEFAULT_CERT_DIR/ --rpath nginx:/etc/ssl/"
  echo "  ${SCRIPT_NAME} cert ispconfig my.site.com --path $M_DEFAULT_CERT_DIR/"
  echo ""
}

function backup {

    local SRC="${1:-}"
    local TARGET="${2:-}"
    local RECIPIENT_OPTION="${3:-}"
    local RECIPIENT="${4:-}"

    ! [[ -z "${TARGET}" ]] || {
        SERVICE_KEY=${SRC}
        SERVICE_KEYS=$(jq ".${SERVICE_KEY}" ${CONFIG} | jq -r 'keys')
        SRC=$(jq ".${SERVICE_KEY}.profile" ${CONFIG} | tr -d '"')
        TARGET=$(jq ".${SERVICE_KEY}.backup" ${CONFIG} | tr -d '"')
        RECIPIENT="vincent.mazenod@isima.fr"
    }

    SRC_DIR=$(dirname "${SRC}")
    SRC_FILE=$(basename -- "${SRC}")

    [[ -d "${SRC}" || -f "${SRC}" ]] || {
        echo "${SRC} Source directory doesn't exist"
        return 1
    }

    TARGET_DIR=$(dirname "${TARGET}")

    [[ -n "${TARGET}" ]] || {
        echo "You must provide a target file"
        echo "try /media/secret-store/gnupg-$(date  +%Y-%m-%d).tgz"
        return 1
    }
    [[ "${TARGET}" =~ .tgz.gpg$ ]] || {
        echo "You must provide a tgz.gpg filename."
        echo "try ${TARGET}.tgz.gpg"
        return 1
    }
    [[ -d "${TARGET_DIR}" ]] || {
        echo "Destination directory doesn't exist"
        return 1
    }

    filename=$(basename -- "${TARGET}")
    TARGET_TAR="${filename%.*}"

    [[ -f "${SRC}" ]] || {
       $(cd "${SRC}" && tar -czf "${TARGET_DIR}/${TARGET_TAR}" . )
    }
    [[ -d "${SRC}" ]] || {
       $(cd "${SRC_DIR}" && tar -czf "${TARGET_DIR}/${TARGET_TAR}" ${SRC_FILE} )
    }
    $(cd "${TARGET_DIR}" && gpg  --batch --yes --encrypt --armor --trust-model always --output "${TARGET_TAR}.gpg" --recipient "${RECIPIENT}" "${TARGET_TAR}")
    $(cd "${TARGET_DIR}" && rm -rf "${TARGET_TAR}")

    echo "Created backup in ${TARGET}"
    echo "To restore it, use"
    echo "    ${SCRIPT_NAME} restore ${TARGET} ${SRC}"

}

function restore {

    local SRC="${1:-}"
    local TARGET="${2:-}"

    ! [[ -z "${TARGET}" ]] || {
        SERVICE_KEY=${SRC}
        SERVICE_KEYS=$(jq ".${SERVICE_KEY}" ${CONFIG} | jq -r 'keys')
        SRC=$(jq ".${SERVICE_KEY}.backup" ${CONFIG} | tr -d '"')
        TARGET=$(jq ".${SERVICE_KEY}.profile" ${CONFIG} | tr -d '"')
    }

    [[ -f "${SRC}" ]] || {
        echo "${SRC} Source file doesn't exist"
        return 1
    }
    [[ "${SRC}" =~ .tgz.gpg$ ]] || {
        echo "You must provide a tgz.gpg filename."
        return 1
    }

    SRC_DIR=$(dirname "${SRC}")

    filename=$(basename -- "${SRC}")
    SRC_TAR="${filename%.*}"

    TARGET_DIR=$(dirname "${TARGET}")

    $(gpg  --output "${SRC_DIR}/${SRC_TAR}" --decrypt "${SRC}")

    TAR_CONTENT=$(tar -tf ${SRC_DIR}/${SRC_TAR})
    if [[ ${TAR_CONTENT} =~ ^\.\/ ]]; then
        $(mkdir -p "${TARGET}")
        $(cd "${TARGET}" && tar -xf "${SRC_DIR}/${SRC_TAR}" .)
    else
        $(cd "${TARGET_DIR}" && tar -xf "${SRC_DIR}/${SRC_TAR}")
    fi

   $(rm -rf "${SRC_DIR}/${SRC_TAR}")

    echo "Restored backup in ${TARGET}"
    echo "To backup it, use"
    echo "    ${SCRIPT_NAME} backup ${TARGET} ${SRC} --recipient mygpgidentity"
}

function backupall {
    SERVICE_KEYS=$(jq "." ${CONFIG} | jq -r 'keys')
    SERVICE_KEYS=$(echo ${SERVICE_KEYS} | sed 's/^\[\(.*\)\]$/\1/')
    SERVICE_KEYS=$(echo ${SERVICE_KEYS} | sed 's/,/ /g')
    for KEY in $SERVICE_KEYS; do
      backup $KEY
    done
}

function restoreall {
    SERVICE_KEYS=$(jq "." ${CONFIG} | jq -r 'keys')
    SERVICE_KEYS=$(echo ${SERVICE_KEYS} | sed 's/^\[\(.*\)\]$/\1/')
    SERVICE_KEYS=$(echo ${SERVICE_KEYS} | sed 's/,/ /g')
    for KEY in $SERVICE_KEYS; do
      restore $KEY
    done
}

function cert_create {

    local DOMAIN="${1:-all}"
    local UNDERSCORED_DOMAIN="${DOMAIN//\./_}"
    local UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN//\*/star}"
    local SUBJ="/C=${C}/ST=${ST}/L=${L}/O=${O}/OU=${OU}/CN=$DOMAIN"
    local TARGET_OPTION="${2:-}"
    local TARGET="${3:-}"

    $(openssl req -new -newkey rsa:2048 -sha256 \
                -nodes -out ${TARGET}/${UNDERSCORED_DOMAIN}.csr -keyout ${TARGET}/${UNDERSCORED_DOMAIN}.key \
                -subj "${SUBJ}")

    $(xed ${TARGET}/${UNDERSCORED_DOMAIN}.csr &)
    $(firefox https://www.digicert.com/secure/requests/ssl_certificate/ssl_multi_domain &)
}

function cert_vault {

    local DOMAIN="${1:-all}"
    local DOMAIN="${DOMAIN//\*/star}"
    local UNDERSCORED_DOMAIN="${DOMAIN//\./_}"
    local SHORT_UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN::-9}"
    local SRC_PATH_OPTION="${2:-}"
    local SRC_PATH="${3:-}"
    local REMOTE_PATH_OPTION="${4:-}"
    local REMOTE_PATH="${5:-}"

    unzip -o "${SRC_PATH}${UNDERSCORED_DOMAIN}.zip" "${UNDERSCORED_DOMAIN}*" -d "${SRC_PATH}"
    CRT=$(cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/${UNDERSCORED_DOMAIN}.crt)
    CA=$(cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/DigiCertCA.crt)
    KEY=$(cat ${SRC_PATH}${UNDERSCORED_DOMAIN}.key)
    CSR=$(cat ${SRC_PATH}${UNDERSCORED_DOMAIN}.csr)
    vault kv put cri/${VAULT_PATH}/${DOMAIN} crt="${CRT}" ca="${CA}" key="${KEY}" csr="${CSR}"
    rm -rf ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/
    

}

function cert_haproxy {

    local DOMAIN="${1:-all}"
    local UNDERSCORED_DOMAIN="${DOMAIN//\./_}"
    local UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN//\*/star}"
    local SHORT_UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN::-9}"
    local SRC_PATH_OPTION="${2:-}"
    local SRC_PATH="${3:-}"
    local REMOTE_PATH_OPTION="${4:-}"
    local REMOTE_PATH="${5:-}"

    unzip -o "${SRC_PATH}${UNDERSCORED_DOMAIN}.zip" -d "${SRC_PATH}"
    cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/${UNDERSCORED_DOMAIN}.crt > ${SRC_PATH}${UNDERSCORED_DOMAIN}.crt
    cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/DigiCertCA.crt >> ${SRC_PATH}${UNDERSCORED_DOMAIN}.crt
    cat ${SRC_PATH}${UNDERSCORED_DOMAIN}.key >> ${SRC_PATH}${UNDERSCORED_DOMAIN}.crt
    rsync -avz --rsync-path="sudo rsync" --chown=root:root --chmod=F600 -e ssh ${SRC_PATH}${UNDERSCORED_DOMAIN}.crt ${REMOTE_PATH}${SHORT_UNDERSCORED_DOMAIN}.crt
    rm -rf ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/
    rm ${SRC_PATH}${UNDERSCORED_DOMAIN}.crt

}

function cert_nginx {

    local DOMAIN="${1:-all}"
    local UNDERSCORED_DOMAIN="${DOMAIN//\./_}"
    local UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN//\*/star}"
    local SHORT_UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN::-9}"
    local SRC_PATH_OPTION="${2:-}"
    local SRC_PATH="${3:-}"
    local REMOTE_PATH_OPTION="${4:-}"
    local REMOTE_PATH="${5:-}"
    local REMOTE_PATH_CERTS="${REMOTE_PATH}certs/"
    local REMOTE_PATH_PRIVATE="${REMOTE_PATH}private/"

    unzip -o "${SRC_PATH}${UNDERSCORED_DOMAIN}.zip" "${UNDERSCORED_DOMAIN}*" -d "${SRC_PATH}"
    cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/${UNDERSCORED_DOMAIN}.crt > ${SRC_PATH}${UNDERSCORED_DOMAIN}.pem
    cat ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/DigiCertCA.crt >> ${SRC_PATH}${UNDERSCORED_DOMAIN}.pem
    rsync -avz --rsync-path="sudo rsync" --chown=root:root --chmod=F600 -e ssh ${SRC_PATH}${UNDERSCORED_DOMAIN}.pem ${REMOTE_PATH_CERTS}${SHORT_UNDERSCORED_DOMAIN}.pem
    rsync -avz --rsync-path="sudo rsync" --chown=root:root --chmod=F600 -e ssh ${SRC_PATH}${UNDERSCORED_DOMAIN}.key ${REMOTE_PATH_PRIVATE}${SHORT_UNDERSCORED_DOMAIN}.key
    rm -rf ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/
    rm ${SRC_PATH}${UNDERSCORED_DOMAIN}.pem

}

function cert_ispconfig {

    local DOMAIN="${1:-all}"
    local UNDERSCORED_DOMAIN="${DOMAIN//\./_}"
    local UNDERSCORED_DOMAIN="${UNDERSCORED_DOMAIN//\*/star}"
    local SRC_PATH_OPTION="${2:-}"
    local SRC_PATH="${3:-}"

    unzip -o "${SRC_PATH}${UNDERSCORED_DOMAIN}.zip" "${UNDERSCORED_DOMAIN}*" -d "${SRC_PATH}"

    [[ -f "${SRC_PATH}${UNDERSCORED_DOMAIN}.key" ]] || {
        echo "Something is missing to deploy tls cert (key?)"
        return 1
    }

    [[ -f "${SRC_PATH}${UNDERSCORED_DOMAIN}.csr" ]] || {
        echo "Something is missing to deploy tls cert (csr?)"
        return 1
    }

    [[ $(ls ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/${UNDERSCORED_DOMAIN}.crt 2>/dev/null) ]] || {
        echo "Something is missing to deploy tls cert (crt, digicert zip?)"
        return 1
    }

    [[ $(ls ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/DigiCertCA.crt 2>/dev/null) ]] || {
        echo "Something is missing to deploy tls cert (DigiCertCA.crt digicert zip?)"
        return 1
    }

    isp cache

    isp sites:web:domain:update $DOMAIN \
        --rewrite-to-https y --ssl y \
        --ssl-state "${ST}" \
        --ssl-locality "${L}" \
        --ssl-organisation "${O}" \
        --ssl-organisation-unit "${OU}" \
        --ssl-country "${C}" \
        --ssl-domain ${DOMAIN} \
        --ssl-key ${SRC_PATH}${UNDERSCORED_DOMAIN}.key \
        --ssl-request ${SRC_PATH}${UNDERSCORED_DOMAIN}.csr \
        --ssl-cert  ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/${UNDERSCORED_DOMAIN}.crt \
        --ssl-bundle ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/DigiCertCA.crt \
        --ssl-action save

    rm -rf ${SRC_PATH}${UNDERSCORED_DOMAIN}_*/

}

function main {

  local COMMAND="${1:-}"

  case "$COMMAND" in
      backup )
          shift
          backup "$@"
          ;;
      backupall )
          shift
          backupall "$@"
          ;;
      restore )
          shift
          restore "$@"
          ;;
      restoreall )
          shift
          restoreall "$@"
          ;;
      cert )
          shift
          SUB_COMMAND="${1:-all}"
          case "$SUB_COMMAND" in
              create )
                shift
                cert_create "$@"
                ;;
              vault )
                shift
                cert_vault "$@"
                ;;
              haproxy )
                shift
                cert_haproxy "$@"
                ;;
              nginx )
                shift
                cert_nginx "$@"
                ;;
              ispconfig )
                shift
                cert_ispconfig "$@"
                ;;
          esac
          ;;
      help )
          usage
          ;;
      * )
          usage
          ;;
  esac
}

main $@
